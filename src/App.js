import {useState} from 'react';
import BurgerView from "./components/BurgerView/BurgerView";
import Ingredients from "./components/Ingredients/Ingredients";
import Counter from "./components/Counter/Counter";
import './components/Framewokrs/bootstrap.min.css';
import salad from "./components/assets/salad.png";
import meat from "./components/assets/meat.png";
import bacon from "./components/assets/bacon.png";
import cheese from "./components/assets/cheese.png";

const App = () => {
    const INGREDIENTS = [
        {name: 'cheese', price: 20, image: cheese},
        {name: 'meat', price: 50, image: meat},
        {name: 'bacon', price: 30, image: bacon},
        {name: 'salad', price: 5, image: salad}
    ]

    const [ingredients, setIngredients] = useState([
        {name:'Cheese', count: 0, id: 0},
        {name:'Meat', count: 0, id: 1},
        {name:'Bacon', count: 0, id: 2},
        {name:'Salad', count: 0, id: 3}
    ]);

    const addIngredients = name => {
        setIngredients(ingredients.map(ing => {
            if (ing.name === name) {
                return {
                    ...ing,
                    count: ing.count + 1
                }
            }
            return ing;
        }))
    }

    const removeIngredient = name => {
        setIngredients(ingredients.map(ing => {
            if (ing.count > 0) {
                if (ing.name === name) {
                    return {
                        ...ing,
                        count: ing.count - 1
                    }
                }
            }
            return ing;
        }))
    }

    return (
        <div className="container">
            <div className="d-flex justify-content-evenly p-3">
                <Ingredients
                    ingredients={INGREDIENTS}
                    ingredientsState={ingredients}
                    addIngredients={addIngredients}
                    removeIng={removeIngredient}
                />
                <BurgerView ingredientsState={ingredients}/>
            </div>
            <Counter ingredients={ingredients} INGREDIENTS={INGREDIENTS}/>
        </div>
    );
};

export default App;