import React, {useState} from 'react';
import './BurgerStyles.css';

const BurgerView = ({ingredientsState}) => {
    return (
        <div className="card p-1 align-self-auto">
            <h3 className="text-center">Burger</h3>
            <div className="Burger">
                <div className="BreadTop">
                    <div className="Seeds1"></div>
                    <div className="Seeds2"></div>
                </div>
                {ingredientsState.map(ing => {
                    const ingredientsAmount = [];

                    for (let i = 0; i < ing.count; i++) {
                        if (ing.name === "Salad") {
                            ingredientsAmount.push(<div key={ing.name} className="Salad"></div>)
                        }

                        if (ing.name === "Cheese"){
                            ingredientsAmount.push(<div key={ing.name} className="Cheese"></div>)
                        }

                        if (ing.name === "Meat") {
                            ingredientsAmount.push(<div key={ing.name} className="Meat"></div>)
                        }

                        if (ing.name === "Bacon") {
                            ingredientsAmount.push(<div key={ing.name} className="Bacon"></div>)
                        }
                    }

                    return ingredientsAmount;
                })}
                <div className="BreadBottom"></div>
            </div>
        </div>
    );
};

export default BurgerView;