import React from 'react';
import '../Framewokrs/bootstrap.min.css';

const Counter = ({ingredients, INGREDIENTS}) => {
    let totalPrice = 20;
    for (let i = 0; i < ingredients.length; i++) {
        totalPrice += (ingredients[i].count * INGREDIENTS[i].price);
    }

    return (
        <div className="d-flex justify-content-center">
            <p className="text-center bg-primary bg-gradient rounded-pill p-3">
                Total price is: {totalPrice}
            </p>
        </div>
    );
};

export default Counter;