import React from "react";
import '../BurgerView/BurgerStyles.css';
import '../Framewokrs/bootstrap.min.css';
import './ingredients.css';

const Ingredients = ({ingredients, ingredientsState, addIngredients, removeIng}) => {
    return (
        <div className="d-flex card flex-column p-1" style={{width: "500px"}}>
            <h3 className="mb-5 text-center">Ingredients</h3>
            {ingredients.map((ing, i) => {
                return (
                    <div id={ing.name} key={ing.name} className="d-flex justify-content-evenly ingredientText px-4 mb-3">
                        <button className="ingBtn" onClick={()=> addIngredients(ingredientsState[i].name)}>
                            <img src={ing.image} width={"50px"} alt="Ingredients"/>
                        </button>
                        <span className="flex-grow-1 mx-4">
                            {ing.name}
                            <span className="px-3">x{ingredientsState[i].count}</span>
                        </span>
                        <button className="removeBtn align-self-center" onClick={()=> removeIng(ingredientsState[i].name)}>
                        </button>
                    </div>
                )
            })}
        </div>
    );
};

export default Ingredients;